$(function () {
  $("[data-toggle='tooltip']").tooltip();
  $("[data-toggle='popover']").popover();
});

$("#btnModal").on("show.bs.modal", function (e) {
  console.log("el modal se está mostrando");

  $("#btnModal").prop("disabled", true);
});

$("#btnModal").on("hidden.bs.modal", function (e) {
  $("#btnModal").prop("disabled", false);
});
